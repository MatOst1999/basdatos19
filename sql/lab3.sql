--1)------------------

--REFACTORIZAR;
CREATE VIEW updated AS
SELECT * 
FROM takes 
WHERE (ID,course_id,sec_id,semester) NOT IN (
	SELECT ID,course_id,sec_id,semester 
	FROM takes 
	WHERE grade = "F" AND (ID,course_id) NOT IN (
		SELECT ID,course_id 
		FROM takes 
		WHERE grade != "F"
		)
	);


--2)------------------

WITH countingF AS (
	SELECT ID,count(grade) as cnt
	FROM updated 
	WHERE grade = "F"
	GROUP BY(ID) 
	)
SELECT ID,course_id,grade 
FROM updated 
WHERE ID IN ( 
	SELECT ID 
	FROM countingF
	WHERE cnt >= 2
	);

--3)------------------

WITH scores AS(
SELECT ID,course_id,sec_id,semester,year,
	CASE WHEN grade LIKE "A%" THEN 10
	WHEN grade LIKE "B%" THEN 8
	WHEN grade LIKE "C%" THEN 6
	WHEN grade LIKE "D%" THEN 4
	WHEN grade LIKE "F%" THEN 0
	END AS score
FROM updated
)

SELECT ID, AVG(score) AS average FROM scores GROUP BY(ID);



--4)------------------

WITH rooms AS(
SELECT DISTINCT room_number, building, sec_id,semester,year 
FROM section 
INNER JOIN 
classroom 
USING(room_number,building)
)

SELECT room_number, building,semester,year,count(sec_id) AS cnt 
FROM rooms 
GROUP BY room_number, building,semester,year 
HAVING cnt > 1 
ORDER BY cnt;

--5)------------------

CREATE VIEW faculty AS
SELECT DISTINCT ID, name, dept_name
FROM instructor;

--6)------------------

CREATE VIEW CSinstructor AS
SELECT * 
FROM instructor
WHERE dept_name = "Comp. Sci."; 

--7)------------------


--NEW referencia en INSERT Y UPDATE al dato insertado y el dato por actualizar respectivamente
--OLD referencia en DELETE Y UPDATE al dato eliminado y el adto sin actualizar respectivamente

DROP TRIGGER IF EXISTS up_cred;

CREATE TRIGGER up_cred 
AFTER INSERT ON takes
FOR EACH ROW 
UPDATE student s, course c SET s.tot_cred = s.tot_cred + c.credits 
WHERE s.ID = NEW.ID AND c.course_id = NEW.course_id;

--8)------------------

--9)------------------

--Los PROCEDURE son los unicos que pueden devolver tablas
--Las FUNCTION devuelven valores

DROP PROCEDURE if exists myProcedure;

CREATE PROCEDURE myProcedure(id int)
   SELECT semester FROM takes WHERE id = takes.ID;


call myProcedure(99977);
