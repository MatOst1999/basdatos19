/*1*/
SELECT min(cantTable.cantidad) AS Min, max(cantTable.cantidad) AS Max
	FROM (  
			SELECT count(ID) AS cantidad
			FROM takes AS t
			GROUP BY t.course_id, t.sec_id, t.semester, t.year
		) AS cantTable;

/*2*/
       
SELECT * FROM
		(SELECT count(ID) AS cantidad, t.course_id, t.sec_id, t.semester, t.year
		FROM takes AS t
		GROUP BY t.course_id, t.sec_id, t.semester, t.year
		ORDER BY cantidad) as cantT
	WHERE cantT.cantidad >= ALL (
		SELECT max(cantTable.cantidad) AS Max
		FROM (  
				SELECT count(ID) AS cantidad
				FROM takes AS t
				GROUP BY t.course_id, t.sec_id, t.semester, t.year
			) AS cantTable);

/*3*/
	/*como no existe ninguna section que no tenga alumnos agrego una*/
	insert into section values ('101', '4', 'Fall', 2025, 'Alumni', '143', 'A');
    
		SELECT min(cantTable.cantidad) AS Min, max(cantTable.cantidad) AS Max
	FROM (  
			SELECT count(ID) AS cantidad
			FROM section AS s
            left join takes as t on s.course_id = t.course_id and s.sec_id = t.sec_id 
            and s.semester = t.semester and s.year = t.year
            GROUP BY s.course_id, s.sec_id, s.semester, s.year
		) AS cantTable;
        
	/* Elimina la section agregada*/
    DELETE FROM section WHERE course_id = '101' and sec_id = '4' and 
    semester = 'Fall' and year = 2025 and building = 'Alumni' and 
    room_number = '143' and time_slot_id = 'A';

--4)------------------

SELECT * FROM course WHERE title LIKE "Comp%";

--5)------------------
---Version 1
WITH comp_courses AS (
SELECT * 
	FROM 
		course 
	WHERE 
		title LIKE "Comp%"
	) 
SELECT 
	instructor.* 
FROM 
	(instructor INNER JOIN 
		(comp_courses INNER JOIN 
			teaches 
		USING(course_id)) 
	USING(ID)
	);


---Version 2
WITH comp_courses AS (
	SELECT * FROM course 
	WHERE title LIKE "Comp%"
	)
SELECT 
	instructor.* 
FROM 
	instructor 
	INNER JOIN teaches USING(ID)
WHERE 
	teaches.course_id IN(
		SELECT 
			course_id 
		FROM 
			comp_courses
		);

--6)------------------
INSERT INTO student (name,dep_name,tot_cred)
SELECT name,dep_name,(SELECT 0) FROM instructor;

--7)------------------
DELETE FROM student 
WHERE ID IN(SELECT ID FROM instructor) AND tot_cred = 0;

--8)------------------
WITH src AS (
	SELECT ID,SUM(credits) AS "sum" 
	FROM takes INNER JOIN course USING(course_id) 
	GROUP BY(ID)
)
UPDATE student s,src SET s.tot_cred = src.sum WHERE s.ID = src.ID;
--9)------------------
 

