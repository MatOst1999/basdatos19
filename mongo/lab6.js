
//1)----------------

db.theaters.aggregate([
	{
		$group:{
			_id:"$location.address.state"
		}
	},
	{
		$count:"total states"
	}
])

//2)----------------

db.theaters.aggregate([
	{
		$group:{
			_id:"$location.address.state",
		 	"No of theaters":{$sum:1}
		}
	}
])

//3)----------------

db.movies.aggregate([
	{
		$match:{
			directors:{$in:["Steven Spielberg"]}
		}
	},
	{
		$count:"Spielberg movies"
	}
])

//4)----------------

db.movies.aggregate([
	{
		$match:{
			year:{
				$gte:1950,
				$lte:1960
			}
		}
	},
	{
		$count: "Oldies"
	}
])

//5)----------------
db.movies.aggregate([
	{
		$project:{
			_id:0,
			genres:1
		}
	},
	{
		$unwind:"$genres"
	},
	{
		$group:{
			_id:"$genres",
			"No of movies":{$sum:1}
		}
	},
	{
		$sort:{
			"No of movies":-1
		}
	},
	{
		$limit:10
	}
])

//6)----------------

db.comments.aggregate([
	{
		$project:{
			_id:0,
			email:1,
			name:1
		}
	},
	{
		$group:{
			_id:{
				name:"$name",
				email:"$email"
			},
			"No of comments":{$sum:1}
		}
	},
	{
		$project:{
			_id:0,
			"name":"$_id.name",
			"email":"$_id.email",
			"No of comments":1
		}
	},
	{
		$sort:{
			"No of comments":-1
		}
	},
	{
		$limit:10
	},
])

//7)----------------

db.movies.aggregate([
	{
		$match:{
			"year": {
				$gte: 1980,
				$lte: 1989
			}
		}
	},
	{
		$project:{
			"_id":0,
			"year":1,
			"rating":"$imdb.rating"
		}
	},
	{
		$group:{
			_id:"$year",
			"min":{
				$min:"$rating"
			},
			"max":{
				$max:"$rating"
			},
			"avg":{
				$avg:"$rating"
			}	
		}
	}
])

//8)----------------


db.comments.aggregate([
	{
		$group:{
			_id:"$movie_id",
			"No of comments":{
				$sum:1
			}
		}
	},
	{
		$sort:{
			"No of comments":-1
		}
	},
	{
		$limit:10
	},
	{
		$lookup:{
			from:"movies",
			localField:"_id",
			foreignField:"_id",
			as: "movie_details"
		}
	},
	{
		$unwind:"$movie_details"
	},
	{
		$project:{
			"_id":0,
			"title":"$movie_details.title",
			"year":"$movie_details.year",
			"No of comments":1
		}
	}
])

//9)----------------

db.movies.aggregate([
	{
		$match:{
			directors:{
				$in:["John Carpenter"]
			}
		}
	},
	{
		$unwind:"$cast"
	},
	{
		$group:{
			_id:"$cast",
			"carpenter_movies":{
				$sum:1
			}
		}
	},
	{
		$match:{
			carpenter_movies:{
				$gte:2
			}
		}
	},
	{
		$lookup:{
			from: "movies",
			localField:"_id",
			foreignField:"cast",
			as: "movies"
		}
	},
	{
		$project:{
			name:"$_id",
			"movies.title":1,
			"movies.year":1
		}
	}
])

//10)----------------


db.comments.aggregate([
	{
		$project:{
			_id:0,
			movie_id:1,
			date:{
				month: {
					$month:"$date"
				},
				year: {
					$year: "$date"
				}
			}
		}
	},
	{
		$group:{
			_id:{
				movie_id:"$movie_id",
				date: "$date"
			},
			"No of comments": {
				$sum:1
			}
		}
	},
	{
		$sort:{
			"No of comments":-1
		}
	}
])

