// 1) ----

db.users.find({"email":"sean_bean@gameofthron.es"},{"password":0})

// 2) ----

db.users.insertOne({"name":"Mateo Ostorero","email":"mateoostorero71@gmail.com","password":"impresora12"})

// 3) ----

db.sessions.updateOne(
    {"user_id":"t3qulfeem@kwiv5.6urr"},
    {
        "$set": {
            "jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9"
        }
    },
    {
        upsert: true
    }

)

// 4) ----

db.sessions.deleteOne({"user_id":"t3qulfeem@kwiv5.6urr"})

// 5) ----

c = ["UK","USA"]

db.movies.find(
	{
		$and:[
			{
				"type":"movie"
			},
			{
				"countries":{
					$in: c
				}
			}
		]
	},
	{
		"_id":1,
		"title":1
	}
).batchSize(10).sort({"title":1})

// 6) ----

db.movies.find(
	{
		$or:[
			{
				"title": /Conspiracy/i

			},
			{
				"plot": /Conspiracy/i
			}
		]
	}
).batchSize(10).sort({"year":1,"title":1})

// 7) ----

genres = ["Short","Drama"]

query = db.movies.find(
	{
		"genres":{
			$in:genres
		}	
	}
)

query.count()

// 8) ----

name = "Mateo Ostorero"
email = "mateoostorero71@gmail.com"
mid = ObjectId("573a1390f29313caabcd418c")
txt = "Huele a obo"
dt =  ISODate("2012-03-26T23:20:16Z")

db.comments.insertOne(
	{
		"name" : name, 
		"email" : email, 
		"movie_id" : mid, 
		"text" : txt, 
		"date" : dt
	}
)

// 9) ----

name = "Mateo Ostorero"
email = "mateoostorero71@gmail.com"
mid = ObjectId("573a1390f29313caabcd418c")
txt = "Huele a obo"
dt =  ISODate("2012-03-26T23:20:16Z")
newtxt = "Ya no huele a obo"
newdt = ISODate("2019-10-14T13:02:00Z")

db.comments.updateOne(
	{
		"email":email,
		"movie_id":mid
	},
	{
		$set:{
			"text":newtxt,
			"date":newdt
		}
	},
	{
		upsert:False
	}
)

// 10) ----

startDate = ISODate("2012-03-26T23:20:16Z")
endDate = ISODate("2019-10-14T13:02:00Z")
mid = ObjectId("573a1390f29313caabcd418c")

db.comments.find(
	{
		$and:[
			{
				"date":{
					$gte: startDate
				}
			},
			{
				"date":{
					$lte: endDate
				}
			},
			{
				"movie_id":mid
			}
		]
	}
).sort({date:1})



